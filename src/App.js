import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import AuthenticateMenu from './components/authenticate/authenticateMenu';
import CustomRoute from './components/authenticate/customRoute';
import Main from './components/webLayout/main';
import './App.css';

class App extends Component {

	constructor(props) {
		super(props);
		const token = localStorage.getItem('token');

		this.state = {
			token
		}
	}

	setToken() {
		this.setState({
			token: localStorage.getItem('token')
		})
	}

	render() {

		return (
			<div className="App">				
				<Router>
					<Switch>
						<CustomRoute path='/arbre' component={Main} redirectPath='/' setToken={() => this.setToken()} authenticateToken={this.state.token} />
						<CustomRoute exact path='/' component={AuthenticateMenu} redirectPath='/arbre' setToken={() => this.setToken()} authenticateToken={!this.state.token} />						
					</Switch>
				</Router>
			</div>
		);
	}
}

export default App;
