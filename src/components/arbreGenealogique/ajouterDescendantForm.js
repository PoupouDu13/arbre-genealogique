import React from 'react';
import { Button, Form, FormGroup, Label, Input, Alert } from 'reactstrap';
import api from '../../services/api';
import SimpleForm from '../common/simpleForm';

export default class AjouterDescendantForm extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			nom: '',
			prenom: '',
			dateNaissance: '',
			dateDeces: '',
			genre: 'homme',
			fileToUpload: '',
		};

		this.handleInputChange = this.handleInputChange.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
	}

	handleInputChange(event) {
		if (event.target.type === 'file') {
			this.setState({
				[event.target.name]: event.target.files
			});
		} else {
			this.setState({
				[event.target.name]: event.target.value
			});
		}
	}

	handleSubmit = async (event) => {
		event.preventDefault();
		let result = '';
		if (this.props.couple) {
			result = await api.addDescendant(
				{
					nom: this.state.nom,
					prenom: this.state.prenom,
					dateNaissance: this.state.dateNaissance,
					dateDeces: this.state.dateDeces,
					genre: this.state.genre,
					couple: this.props.couple
				}
			);
		} else {
			result = await api.addFirstPersonne(
				{
					nom: this.state.nom,
					prenom: this.state.prenom,
					dateNaissance: this.state.dateNaissance,
					dateDeces: this.state.dateDeces,
					genre: this.state.genre,
				}
			);
		}
		if (this.state.fileToUpload !== '') {
			const uploadResult = await api.uploadPhoto({ files: this.state.fileToUpload, personneId: result.data.id });
			if (uploadResult.message) {
				this.setState({
					error: uploadResult.message,
				})
			}
		}

		const apiResult = await api.getTree();
		this.props.handleSetData({ data: apiResult.data });		
		this.props.getTree();
	}

	render() {
		return (
			<SimpleForm
				handleSubmit={this.handleSubmit}
				handleInputChange={this.handleInputChange}
				error={this.state.error}
			/>
		);
	}
}