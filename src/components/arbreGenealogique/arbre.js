import React from 'react';
import faPlus from '@fortawesome/fontawesome-free-solid/faPlus';
import { Button } from 'reactstrap';
import { withRouter } from 'react-router-dom';
import axios from 'axios';
import Couple from './couple';
import Api from '../../services/api';
import Maths from '../../helper/maths';
import SimpleModal from '../common/simpleModal';
import LoadingScreen from '../webLayout/loadingScreen';
import AjouterDescendantForm from './ajouterDescendantForm';
import Celibataire from './celibataire';
import interceptorsHelper from '../../helper/interceptorsHelper';
import './css/personne.css';

class Arbre extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            interceptors: {},
            width: this.props.initialWidth
        }

        this.getTree = this.getTree.bind(this);
        this.deletePersonneMarier = this.deletePersonneMarier.bind(this);
        this.deletePersonneCelibataire = this.deletePersonneCelibataire.bind(this);
        this.setWidth = this.setWidth.bind(this);
    }

    componentDidMount() {
        const activeInterceptors = interceptorsHelper.setInterceptors(
            {
                callBacks:
                    {
                        requestCallback: () => this.setState({ loading: true }),
                        responseCallback: () => this.setState({ loading: false })
                    }
            }
        );
        this.setState({
            interceptors: activeInterceptors,
        });
        this.getTree();
        this.setWidth();
    }

    componentWillUnmount() {
        interceptorsHelper.removeInterceptors({ activeInterceptors: this.state.interceptors })
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.width !== this.state.width) {
            this.setState({ width: nextProps.width });
            this.setWidth();
        }
    }

    setWidth() {
        const domTree = document.getElementById('FamilyTreeDiv');
        (domTree) ? domTree.style.width = `${this.state.width}px` : null;
    }

    getTree() {
        const people = this.formatData(this.props.data);
        this.setState({
            couple: people.couple || null,
            personne: people.personne || null,
        });
    }

    deletePersonneMarier = async ({ id }) => {
        await Api.deletePersonneMarier({ id });
        const result = await Api.getTree();
        this.props.handleSetData({ data: result.data });
        this.getTree();
    }

    deletePersonneCelibataire = async ({ id }) => {
        await Api.deletePersonneCelibataire({ id });
        const result = await Api.getTree();
        this.props.handleSetData({ data: result.data });
        this.getTree();
    }

    formatData(data) {
        const firstCouple = this.getGenerationCouple({ data, generationId: 0 })[0];
        if (firstCouple) {
            this.handleCouple({ data, generationId: 0, couple: firstCouple });

            return { couple: firstCouple };
        } else {
            return { personne: this.makePersonne(data.nodes.find(node => node.generationId === 0)) }
        }
    }

    handleCouple({ data, generationId, couple }) {
        const childrenOfPersonne1 = this.getChildren({ couple, data });
        const nextcouples = this.getGenerationCouple({ data, generationId: generationId + 1 });
        for (let i = 0; i < childrenOfPersonne1.length; i++) {
            const childOfPersonne1 = childrenOfPersonne1[i];
            const childCoupleOfPersonne1 = nextcouples.find(couple => couple.personne1.id === childOfPersonne1.id || couple.personne2.id === childOfPersonne1.id)
            if (childCoupleOfPersonne1) {
                couple.descendants = couple.descendants || { personnes: [], couples: [] };
                couple.descendants.couples.push(childCoupleOfPersonne1);
                this.handleCouple({ data, generationId: generationId + 1, couple: childCoupleOfPersonne1 });
            } else {
                couple.descendants = couple.descendants || { personnes: [], couples: [] };
                couple.descendants.personnes.push(childOfPersonne1);
            }
        }
    }

    getChildren({ couple, data }) {
        const childOfLinks = data.links.filter(link => link.type === 'ENFANT_DE');
        const personne1Node = data.nodes.find(node => node.id === couple.personne1.id);
        const childrenOfPersonne1Links = childOfLinks.filter(link => link.target === personne1Node.id);
        const childrenOfPersonne1 = [];
        for (let k = 0; k < childrenOfPersonne1Links.length; k++) {
            const childrenOfPersonne1Link = childrenOfPersonne1Links[k];
            const childrenOfPersonne1Node = data.nodes.find(node => node.id === childrenOfPersonne1Link.source);
            childrenOfPersonne1.push(this.makePersonne(childrenOfPersonne1Node));
        }
        return childrenOfPersonne1;
    }

    getGenerationCouple({ data, generationId }) {
        const generationNodes = data.nodes.filter(node => node.generationId === generationId);
        const mariageLinks = data.links.filter(link => link.type === 'MARIER');
        const generationCouples = [];
        for (let i = 0; i < mariageLinks.length; i++) {
            const mariageLink = mariageLinks[i];
            const mariedCouple = this.makeCouple(mariageLink, generationNodes);
            (mariedCouple) ? generationCouples.push(mariedCouple) : null;
        }

        return generationCouples;
    }

    makeCouple(link, nodes) {
        const guid = Maths.guidGenerator();
        const personne1 = this.makePersonne(nodes.find(node => node.id === link.source));
        const personne2 = this.makePersonne(nodes.find(node => node.id === link.target));
        if (personne1 && personne2) {
            return {
                id: guid,
                personne1: personne1,
                personne2: personne2,
            }
        }
    }

    makePersonne(node) {
        if (node) {
            const personne = node;
            personne.id = node.id || node.id.low;
            return personne;
        }
    }

    render() {
        return (
            <LoadingScreen loading={this.state.loading}>
                <div>
                    <div className='tree' id='FamilyTreeDiv'>
                        {
                            (this.state.couple) ?
                                <ul>
                                    <Couple
                                        couple={this.state.couple}
                                        getTree={this.getTree}
                                        deletePersonneMarier={this.deletePersonneMarier}
                                        deletePersonneCelibataire={this.deletePersonneCelibataire}
                                        addDefaultSrc={this.addDefaultSrc}
                                        handleSetData={this.props.handleSetData}
                                    />
                                </ul>
                                : (this.state.personne)
                                    ?
                                    <Celibataire personne={this.state.personne}
                                        getTree={this.getTree}
                                        deletePersonneCelibataire={() => this.deletePersonneCelibataire({ id: this.state.personne.id })}
                                        addDefaultSrc={this.addDefaultSrc}
                                        handleSetData={this.props.handleSetData}
                                    />
                                    :
                                    <SimpleModal modalTitle='Ajout de la première personne' icon={faPlus} >
                                        <AjouterDescendantForm
                                            getTree={this.getTree}
                                            handleSetData={this.props.handleSetData}
                                        />
                                    </SimpleModal>
                        }
                    </div>
                </div>
            </LoadingScreen>
        );
    }
}

export default withRouter(Arbre);