import React from 'react';
import { Container, Row, Col, Button } from 'reactstrap';
import SimpleModal from '../common/simpleModal';
import AjouterPartenaireForm from './ajouterPartenaireForm';
import Portrait from './portrait';
import ModifierPersonneForm from './modifierPersonneForm';
import InfoPersonne from './infoPersonne';
import FontAwesomeIcon from '@fortawesome/react-fontawesome';
import faTimes from '@fortawesome/fontawesome-free-solid/faTimes';
import faHeart from '@fortawesome/fontawesome-free-solid/faHeart';
import faEdit from '@fortawesome/fontawesome-free-solid/faEdit';
import './css/personne.css';

class Celibataire extends React.Component {

    render() {
        return (
            <li>
                <div className='personne'>
                    <Row>
                        <span className={(this.props.personne.genre === 'homme') ? 'male' : 'female'}>
                            <InfoPersonne personne={this.props.personne}>
                                <Col>
                                    <Button onClick={() => { this.props.deletePersonneCelibataire({ id: this.props.personne.id }) }}>
                                        <FontAwesomeIcon icon={faTimes} />
                                    </Button>
                                    <SimpleModal modalTitle='Ajouter un partenaire' icon={faHeart}>
                                        <AjouterPartenaireForm
                                            handleSetData={this.props.handleSetData}
                                            personne={this.props.personne}
                                            getTree={this.props.getTree} />
                                    </SimpleModal>
                                    <SimpleModal modalTitle='Modifier' icon={faEdit}>
                                        <ModifierPersonneForm
                                            personne={this.props.personne}
                                            getTree={this.props.getTree}
                                            handleSetData={this.props.handleSetData}
                                        />
                                    </SimpleModal>
                                </Col>
                            </InfoPersonne>
                        </span>
                    </Row>
                </div>
            </li>
        );
    }
}

export default Celibataire;
