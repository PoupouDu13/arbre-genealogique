import React from 'react';
import { Container, Row, Col, Button } from 'reactstrap'
import Personne from './personne';
import Celibataire from './celibataire';
import SimpleModal from '../common/simpleModal';
import AjouterDescendantForm from './ajouterDescendantForm';
import api from '../../services/api';
import faPlus from '@fortawesome/fontawesome-free-solid/faPlus';
import './css/personne.css';

class Couple extends React.Component {

    render() {
        return (
            <li>
                <div className='personne'>
                    <Row>
                        <Personne
                            personne={this.props.couple.personne1}
                            getTree={this.props.getTree}
                            addDefaultSrc={this.props.addDefaultSrc}
                            handleSetData={this.props.handleSetData}
                        />
                        <span className="spacer"></span>
                        <Personne
                            personne={this.props.couple.personne2}
                            getTree={this.props.getTree}
                            addDefaultSrc={this.props.addDefaultSrc}
                            deletePersonneMarier={(this.props.couple.descendants) ? null : () => { this.props.deletePersonneMarier({ id: this.props.couple.personne2.id }) }}
                            handleSetData={this.props.handleSetData}
                        />
                    </Row>
                    <Row>
                        <Col>
                            <SimpleModal modalTitle='Ajouter un descendant' icon={faPlus} >
                                <AjouterDescendantForm
                                    couple={this.props.couple}
                                    getTree={this.props.getTree}
                                    handleSetData={this.props.handleSetData}
                                />
                            </SimpleModal>
                        </Col>
                    </Row>
                </div>
                <ul>
                    {
                        this.props.couple.descendants &&
                        this.props.couple.descendants.couples.map(couple =>
                            <Couple
                                key={couple.id}
                                couple={couple}
                                getTree={this.props.getTree}
                                deletePersonneMarier={this.props.deletePersonneMarier}
                                deletePersonneCelibataire={this.props.deletePersonneCelibataire}
                                addDefaultSrc={this.props.addDefaultSrc}
                                handleSetData={this.props.handleSetData}
                            />
                        )
                    }
                    {
                        this.props.couple.descendants &&
                        this.props.couple.descendants.personnes.map(personne =>
                            <Celibataire
                                key={personne.id}
                                personne={personne}
                                getTree={this.props.getTree}
                                deletePersonneCelibataire={this.props.deletePersonneCelibataire}
                                addDefaultSrc={this.props.addDefaultSrc}
                                handleSetData={this.props.handleSetData}
                            />
                        )
                    }
                </ul>
            </li>
        );
    }
}

export default Couple;
