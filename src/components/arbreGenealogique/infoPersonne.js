import React from 'react';
import { Container, Row, Col, Button } from 'reactstrap';
import Portrait from './portrait';

class Personne extends React.Component {

    render() {
        return (
            <Col>
                <Col>
                    <Portrait id={this.props.personne.id} />
                </Col>
                <Col>
                    <Row>
                        <Col>
                            {this.props.personne.nom}
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            {this.props.personne.prenom}
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            {this.props.personne.dateNaissance}
                        </Col>
                        <Col>
                            {this.props.personne.dateDeces}
                        </Col>
                    </Row>
                    <Row>
                        {this.props.children}
                    </Row>
                </Col>
            </Col>
        );
    }
}

export default Personne;
