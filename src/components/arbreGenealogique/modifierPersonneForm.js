import React from 'react';
import { Button, Form, FormGroup, Label, Input } from 'reactstrap';
import api from '../../services/api';
import SimpleForm from '../common/simpleForm';

export default class ModifierPersonneForm extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            nom: this.props.personne.nom,
            prenom: this.props.personne.prenom,
            dateNaissance: this.props.personne.dateNaissance,
            dateDeces: this.props.personne.dateDeces,
            genre: this.props.personne.genre,
            fileToUpload: '',
        };

        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleInputChange(event) {
        if (event.target.type === 'file') {
            this.setState({
                [event.target.name]: event.target.files
            });
        } else {
            this.setState({
                [event.target.name]: event.target.value
            });
        }
    }

    handleSubmit = async (event) => {
        event.preventDefault();
        const result = await api.modifiePersonne(
            {
                id: this.props.personne.id,
                nom: this.state.nom,
                prenom: this.state.prenom,
                dateNaissance: this.state.dateNaissance,
                dateDeces: this.state.dateDeces,
                genre: this.state.genre,
            }
        );
        (this.state.fileToUpload !== '') ? await api.uploadPhoto({ files: this.state.fileToUpload, personneId: result.data.id }) : null;
		const apiResult = await api.getTree();
		this.props.handleSetData({ data: apiResult.data });		
		this.props.getTree();
    }

    render() {
        return (
            <SimpleForm
                handleSubmit={this.handleSubmit}
                handleInputChange={this.handleInputChange}
                personne={this.props.personne}
            />
        );
    }
}