import React from 'react';
import { Container, Row, Col, Button } from 'reactstrap';
import api from '../../services/api';
import FontAwesomeIcon from '@fortawesome/react-fontawesome';
import faTimes from '@fortawesome/fontawesome-free-solid/faTimes';
import ModifierPersonneForm from './modifierPersonneForm';
import SimpleModal from '../common/simpleModal';
import InfoPersonne from './infoPersonne';
import faEdit from '@fortawesome/fontawesome-free-solid/faEdit';
import './css/personne.css';

class Personne extends React.Component {

    render() {
        return (
            <span className={(this.props.personne.genre === 'homme') ? 'male' : 'female'}>
                <InfoPersonne personne={this.props.personne}>
                    <Col>
                        {
                            (this.props.deletePersonneMarier) &&
                            <Button onClick={this.props.deletePersonneMarier}>
                                <FontAwesomeIcon icon={faTimes} />
                            </Button>
                        }
                        <SimpleModal modalTitle='Modifier' icon={faEdit}>
                            <ModifierPersonneForm
                                personne={this.props.personne}
                                getTree={this.props.getTree}
                                handleSetData={this.props.handleSetData}
                            />
                        </SimpleModal>
                    </Col>
                </InfoPersonne>
            </span>
        );
    }
}

export default Personne;
