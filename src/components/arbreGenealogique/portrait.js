import React from 'react';
import TokenStorage from '../../helper/tokenStorage';

class Portrait extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }

    addDefaultSrc(ev) {
        ev.target.src = `${process.env.REACT_APP_API_URL || 'http://localhost:8100'}/portraits/default.png`;
    }

    render() {
        return (
            <img className='photo' src={`${process.env.REACT_APP_API_URL || 'http://localhost:8100'}/portraits/${TokenStorage.decodeToken().id}/${this.props.id}.png`} onError={(ev) => this.addDefaultSrc(ev)} / >
        );
    }
}

export default Portrait;