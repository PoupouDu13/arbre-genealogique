import React from 'react';
import { Container, Row, Col, Button } from 'reactstrap';
import FontAwesomeIcon from '@fortawesome/react-fontawesome';
import faEye from '@fortawesome/fontawesome-free-solid/faEye';
import faUsers from '@fortawesome/fontawesome-free-solid/faUsers';
import faTree from '@fortawesome/fontawesome-free-solid/faTree';
import axios from 'axios';
import SimpleModal from '../common/simpleModal';
import LoginForm from './loginForm';
import NewUserForm from './newUserForm';
import LoadingScreen from '../webLayout/loadingScreen';
import interceptorsHelper from '../../helper/interceptorsHelper';
import './css/authenticateMenuCss.css'
import customTree from '../../assets/tree.svg';
const style = {
    backgroundImage: `url(${customTree})`,
    backgroundSize: 'cover',
    overflow: 'hidden',
    backgroundPosition: 'center',
    backgroundColor: 'green',
}

class AuthenticateMenu extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            interceptors: {},
        };
    }

    componentDidMount() {
        const activeInterceptors = interceptorsHelper.setInterceptors(
            {
                callBacks:
                {
                    requestCallback: () => this.setState({ loading: true }),
                    responseCallback: () => this.setState({ loading: false })
                }
            }
        );
        this.setState({
            interceptors: activeInterceptors,
        });
    }

    componentWillUnmount() {
        interceptorsHelper.removeInterceptors({ activeInterceptors: this.state.interceptors })
    }

    render() {
        return (
            <LoadingScreen loading={this.state.loading}>
                <Col style={style}>
                    <Row className='h-100'>
                        <Col sm={{ size: 6, offset: 3 }} className='d-flex align-content-center flex-wrap alignChromeItems'>
                            <div className="d-flex flex-column">
                                <div className="p-2 homeLine">
                                    <FontAwesomeIcon className='iconFormat' icon={faTree} />
                                    Construisez votre arbre généalogique
                                </div>
                                <div className="p-2 homeLine">
                                    <FontAwesomeIcon className='iconFormat' icon={faEye} />
                                    Visualisez vos liens familiaux
                                </div>
                                <div className="p-2 homeLine">
                                    <FontAwesomeIcon className='iconFormat' icon={faUsers} />
                                    Partagez
                                </div>
                            </div>
                        </Col>
                    </Row>
                    <div className='bottom'>Icons made by <a href="https://www.flaticon.com/authors/freepik" title="Tree with thin branches covered by leaves">Tree with thin branches covered by leaves</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a></div>
                </Col>
                <Col>
                    <Row className='h-100'>
                        <div className='loginLine'>
                            <LoginForm setToken={this.props.setToken} formLogInLine='formLogInLine' submitLabel="Se Connecter" authFailed={() => this.setState({ loading: false })} />
                        </div>
                        <Col sm={{ size: 6, offset: 3 }} className='d-flex align-content-center flex-wrap alignChromeItems'>
                            <div className="d-flex flex-column bigText">
                                <div className="p-2">
                                    <img src={customTree} alt='tree' className='smallHeight' />
                                </div>
                                <div className="p-2 maginBottomHomeButton">
                                    Commencer à creer votre arbre généalogique
                                    </div>
                                <div className="p-2 fullWidth">
                                    <SimpleModal buttonLabel='Nouvel utilisateur' fullWidth='fullWidth' homeButton='homeButton' marginHome='marginHome' modalTitle='Créer votre compte' color="success">
                                        <NewUserForm submitLabel="Créer son arbre" setToken={this.props.setToken} />
                                    </SimpleModal>
                                    <SimpleModal buttonLabel='Identification' fullWidth='fullWidth' homeButton='homeButton' marginHome='marginHome' modalTitle='Se connecter' loginButton='loginButton' >
                                        <LoginForm setToken={this.props.setToken} fullWidth='fullWidth' submitLabel="Se Connecter" authFailed={() => this.setState({ loading: false })} />
                                    </SimpleModal>
                                </div>
                            </div>
                        </Col>
                    </Row>
                </Col>
            </LoadingScreen>
        );
    }
}

export default AuthenticateMenu;