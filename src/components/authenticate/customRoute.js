import React from "react";
import { Route, Redirect, withRouter } from "react-router-dom";

const CustomRoute = ({ component: Component, redirectPath, setToken, authenticateToken, ...rest }) => (
    <Route
            {...rest}
            render={props =>
                authenticateToken ? (
                    <Component {...props} setToken={setToken}/>
                ) : (
                        <Redirect
                            to={{
                                pathname: redirectPath,
                                state: { from: props.location }
                            }}
                        />
                    )
            }
        />
    );

export default withRouter(CustomRoute);