import React from 'react';
import { Button, Form, FormGroup, Label, Input, Alert } from 'reactstrap';
import { withRouter } from 'react-router-dom';
import authenticateApi from '../../services/authenticateApi';

class LoginForm extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			username: '',
			password: '',
			error: ''
		};

		this.handleInputChange = this.handleInputChange.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
	}

	handleInputChange(event) {
		this.setState({
			[event.target.name]: event.target.value
		});
	}

	handleSubmit(event) {
		event.preventDefault();
		const usernameInput = this.state.username;
		const passwordInput = this.state.password;		
		authenticateApi.login({ username: usernameInput, password: passwordInput })
			.then((user) => {				
				this.props.setToken();
				this.props.history.push({
					pathname: '/arbre',
					state: { data: user.id },
				});
			})
			.catch((err) => {				
				this.props.authFailed();
				alert(err.response ? err.response.data.message : err.message);
			})
	}

	render() {
		return (
			<Form onSubmit={this.handleSubmit} className={(this.props.formLogInLine) ? 'form-inline' : null}>
				<FormGroup>
					<Input type="text" name="username" id="userNameNewUserForm" placeholder="Nom d'utilisateur" onChange={this.handleInputChange} />
				</FormGroup>
				<FormGroup>
					<Input type="password" name="password" id="passwordNewUserForm" placeholder="Mot de passe" className={(this.props.formLogInLine) ? 'margin-right-left' : null} onChange={this.handleInputChange} />
				</FormGroup>
				<Button color='success' className={`homeButton ${this.props.fullWidth}`}>{this.props.submitLabel}</Button>
				{this.state.error && (
					<Alert color="danger">{this.state.error}</Alert>
				)}
			</Form>
		);
	}
}

export default withRouter(LoginForm);