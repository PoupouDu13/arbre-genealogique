import React from 'react';
import { withRouter } from 'react-router-dom';
import { Button, Form, FormGroup, Label, Input } from 'reactstrap';
import authenticateApi from '../../services/authenticateApi';

class NewUserForm extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			username: '',
			email: '',
			password: '',
		};

		this.handleInputChange = this.handleInputChange.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
	}

	handleInputChange(event) {
		this.setState({
			[event.target.name]: event.target.value
		});
	}

	handleSubmit(event) {
		event.preventDefault();
		const usernameInput = this.state.username;
		const passwordInput = this.state.password;
		const emailInput = this.state.email;		
		authenticateApi.subscribe({ username: usernameInput, email: emailInput, password: passwordInput }).then((response) => {
			if (response.message) {
				alert(response.message);
			} else {				
				this.props.setToken();
				this.props.history.push({
					pathname: '/arbre',
					state: { data: response.id },
				});
			}
		});
	}

	render() {
		return (
			<Form onSubmit={this.handleSubmit} >
				<FormGroup>
					<Input type="text" name="username" id="userNameNewUserForm" placeholder="Nom d'utilisateur" onChange={this.handleInputChange} required />
				</FormGroup>
				<FormGroup>
					<Input type="email" name="email" id="emailNewUserForm" placeholder="Email" onChange={this.handleInputChange} required />
				</FormGroup>
				<FormGroup>
					<Input type="password" name="password" id="passwordNewUserForm" placeholder="Mot de passe" onChange={this.handleInputChange} required />
				</FormGroup>
				<Button color='success' className='homeButton fullWidth'>{this.props.submitLabel}</Button>
			</Form>
		);
	}
}

export default withRouter(NewUserForm);