import React from 'react';
import { Button, Form, FormGroup, Label, Input, Alert } from 'reactstrap';

class SimpleForm extends React.Component {

	render() {
		return (
			<Form onSubmit={this.props.handleSubmit}  >
				{
					this.props.error &&
					<Alert color="danger"> {this.props.error} </Alert>
				}
				<FormGroup>
					<Label for="nom">Nom</Label>
					<Input
						type="text"
						name="nom"
						id="nomInput"
						placeholder="example: Derrien"
						onChange={this.props.handleInputChange}
						defaultValue={(this.props.personne) ? this.props.personne.nom : null}
						required
					/>
				</FormGroup>
				<FormGroup>
					<Label for="prenom">Prénom</Label>
					<Input
						type="text"
						name="prenom"
						id="prenomInput"
						placeholder="example: potiron"
						onChange={this.props.handleInputChange}
						required
						defaultValue={(this.props.personne) ? this.props.personne.prenom : null}
					/>
				</FormGroup>
				<FormGroup>
					<Label for="dateNaissance">Date naissance</Label>
					<Input
						type="date"
						name="dateNaissance"
						id="dateNaissanceInput"
						onChange={this.props.handleInputChange}
						defaultValue={(this.props.personne) ? this.props.personne.dateNaissance : null}
					/>
				</FormGroup>
				<FormGroup>
					<Label for="dateDeces">Date deces</Label>
					<Input
						type="date"
						name="dateDeces"
						id="dateDecesInput"
						onChange={this.props.handleInputChange}
						defaultValue={(this.props.personne) ? this.props.personne.dateDeces : null}
					/>
				</FormGroup>
				<FormGroup>
					<Label for="genre">Select</Label>
					<Input
						type="select"
						name="genre"
						id="genreInput"
						onChange={this.props.handleInputChange}
						defaultValue={(this.props.personne) ? this.props.personne.genre : null}
					>
						<option>homme</option>
						<option>femme</option>
					</Input>
				</FormGroup>
				<FormGroup>
					<Label for="fileToUpload">Portrait au format png</Label>
					<Input
						type="file"
						name="fileToUpload"
						id="fileToUploadInput"
						placeholder="example: une photo de potiron"
						accept="image/*"
						onChange={this.props.handleInputChange}
					/>
				</FormGroup>
				<Button color='danger'>Submit</Button>
			</Form>
		);
	}
}

export default SimpleForm;