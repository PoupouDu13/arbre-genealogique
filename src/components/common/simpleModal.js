import React from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import FontAwesomeIcon from '@fortawesome/react-fontawesome';


class SimpleModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            modal: false
        };

        this.toggle = this.toggle.bind(this);
    }

    toggle() {
        this.setState({
            modal: !this.state.modal
        });
    }

    render() {
        return (
            <span>
                <Button color={this.props.color} onClick={this.toggle} className={`${this.props.fullWidth} ${this.props.homeButton} ${this.props.marginHome} ${this.props.loginButton}`}>{this.props.buttonLabel || <FontAwesomeIcon icon={this.props.icon} />} </Button>
                <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
                    <ModalHeader toggle={this.toggle}>{this.props.modalTitle}</ModalHeader>
                    <ModalBody>
                        {this.props.children}
                    </ModalBody>
                    <ModalFooter>
                        <Button color="secondary" onClick={this.toggle} className={this.props.homeButton}>Cancel</Button>
                    </ModalFooter>
                </Modal>
            </span>
        );
    }
}

export default SimpleModal;