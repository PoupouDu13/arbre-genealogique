import React from 'react';
import { Container, Row, Col } from 'reactstrap';
import customTree from '../../assets/tree.svg';

class LoadingScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }

    render() {
        return (
            <Container className='h-100' fluid={true}>
                {
                    (this.props.loading) ?
                        <Row className='h-100'>
                            <Col sm={{ size: 6, offset: 3 }} className='d-flex align-content-center flex-wrap'>
                                <div className="d-flex flex-column">
                                    <div className="p-2 homeLine">
                                        <img src={customTree} alt='tree' />
                                    </div>
                                </div>
                            </Col>
                        </Row>
                        :
                        <Row className='h-100'>
                            {this.props.children}
                        </Row>
                }
            </Container>
        );
    }
}

export default LoadingScreen;