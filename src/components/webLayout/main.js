import React from 'react';
import { Button, Container } from 'reactstrap';
import MainHeader from './mainHeader';
import Arbre from '../arbreGenealogique/arbre';
import TokenStorage from '../../helper/tokenStorage';
import Api from '../../services/api';

class Main extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            initialWidth: 50,
        }

        this.handleSetWidth = this.handleSetWidth.bind(this);
        this.calculateWidth = this.calculateWidth.bind(this);
        this.handleSetData = this.handleSetData.bind(this);
    }

    componentDidMount() {
        Api.getTree().then((result) => {
            this.handleSetData({ data: result.data });            
        });
    }

    handleSetData({ data }) {
        this.setState({
            data
        });        
    }

    handleSetWidth({ width }) {
        this.setState({
            width: this.calculateWidth({ width })
        });
    }

    getMaxPersonneNumber({ data }) {
        let maxPersonneNumber = 0;
        let current = 0;
        let maxGenerationId = 0;
        for (let i = 0; i < data.maxDepth + 1; i++) {
            current = data.nodes.filter(node => node.generationId === i).length;
            maxPersonneNumber = (current > maxPersonneNumber) ? current : maxPersonneNumber;
            maxGenerationId = (i > maxGenerationId) ? i : maxGenerationId;
        }

        return { maxPersonneNumber, maxGenerationId };
    }

    calculateWidth({ width }) {
        const maxPersonneData = this.getMaxPersonneNumber({ data: this.state.data });
        return maxPersonneData.maxPersonneNumber * 40 + maxPersonneData.maxGenerationId * 40 * width;
    }

    render() {
        return (
            <Container fluid={true}>
                {
                    (this.state.data) &&
                    [
                        <MainHeader key="1" profil={TokenStorage.decodeToken()} setToken={this.props.setToken} setWidth={this.handleSetWidth} initialWidth={this.state.initialWidth} />,
                        <Arbre key="2" data={this.state.data} handleSetData={this.handleSetData} width={this.state.width} initialWidth={this.calculateWidth({ width: this.state.initialWidth })} />
                    ]
                }
            </Container>
        );
    }
}

export default Main;