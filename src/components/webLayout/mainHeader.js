import React from 'react';
import { Button, Col, Row } from 'reactstrap';
import { withRouter } from 'react-router-dom';
import authenticateApi from '../../services/authenticateApi';
import WidthSlider from './widthSlider';
import './css/mainHeader.css';

class MainHeader extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }

    logout() {
        authenticateApi.logout();
        this.props.history.push({
            pathname: '/',
        });
        this.props.setToken();
    }

    render() {
        return (
            <Row className='mainHeader d-flex align-content-center flex-wrap'>
                <Col sm='3'>
                    <Button onClick={() => this.logout()} className='homeButton mainHeaderLogOutButton'>
                        Déconnexion
                    </Button>
                </Col>
                <Col sm='6' className='mainHeaderText'>
                    Arbre généalogique de : {this.props.profil.username}
                </Col>
                <Col>
                    {
                        this.props.setWidth &&
                        <WidthSlider setWidth={this.props.setWidth} initialWidth={this.props.initialWidth} />
                    }
                </Col>
            </Row>
        );
    }
}

export default withRouter(MainHeader);