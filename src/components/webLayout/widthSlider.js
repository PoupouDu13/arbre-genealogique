import React, { Component } from 'react'
import Slider from 'react-rangeslider'

class WidthSlider extends Component {
    constructor(props, context) {
        super(props, context)
        this.state = {
            width: this.props.initialWidth
        }
    }

    componentDidMount() {
        this.props.setWidth({
            width: this.state.width
        });        
    }

    handleOnChange = (value) => {
        this.setState({
            width: value
        })
        this.props.setWidth({
            width: value
        });
    }

    render() {
        let { width } = this.state
        return (
            <Slider
                value={width}
                onChange={this.handleOnChange}
            />
        )
    }
}

export default WidthSlider;