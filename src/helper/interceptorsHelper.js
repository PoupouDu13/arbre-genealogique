import axios from 'axios';

const setInterceptors = ({ callBacks }) => {
    const activeInterceptors = {};
    activeInterceptors.request = axios.interceptors.request.use((config) => {
        callBacks.requestCallback();

        return config;
    });
    activeInterceptors.response = axios.interceptors.response.use((response) => {
        callBacks.responseCallback();

        return response;
    });    
    
    return activeInterceptors;
}

const removeInterceptors = ({activeInterceptors}) => {    
    axios.interceptors.request.eject(activeInterceptors.request);
    axios.interceptors.response.eject(activeInterceptors.response);        
}

export default {
    setInterceptors,
    removeInterceptors,
}