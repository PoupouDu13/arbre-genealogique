const Maths = {};

Maths.entierAleatoire = (min, max) => {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

Maths.guidGenerator = () => {
    const S4 = function () {
        return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
    };
    return (S4() + S4() + "-" + S4() + "-" + S4() + "-" + S4() + "-" + S4() + S4() + S4());
}

Maths.max = (int1, int2) => {
    return (int1 > int2) ? int1 : int2;
}

Maths.min = (int1, int2) => {
    return (int1 < int2) ? int1 : int2;
}

Maths.getSum = (total, num) => {
    return total + num;
}

Maths.handleEmpty = (value) => {
    return (value === '') ? null : value;
}

module.exports = Maths;
