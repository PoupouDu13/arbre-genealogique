import decode from 'jwt-decode';

const TokenStorage = {};

TokenStorage.getToken = () => {
    return localStorage.getItem('token');
}

TokenStorage.decodeToken = () => {
    return decode(TokenStorage.getToken());
}

export default TokenStorage;