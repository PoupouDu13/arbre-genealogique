import axios from 'axios';
import Maths from '../helper/maths';
import TokenStorage from '../helper/tokenStorage';

const API_URL = process.env.REACT_APP_API_URL || 'http://localhost:8100';

axios.interceptors.request.use((config) => {
    const newConfig = { ...config };
    newConfig.data.dateDeces = Maths.handleEmpty(newConfig.data.dateDeces);
    newConfig.data.dateNaissance = Maths.handleEmpty(newConfig.data.dateNaissance);

    return newConfig;
})

const addDescendant = async ({ nom, prenom, dateNaissance, dateDeces, genre, couple }) => {
    try {
        const id = Maths.guidGenerator();
        const response = await axios.post(`${API_URL}/api/personne/addDescendant`,
            {
                nom,
                prenom,
                dateNaissance,
                dateDeces,
                genre,
                personne1Id: couple.personne1.id,
                personne2Id: couple.personne2.id,
                generationId: couple.personne1.generationId + 1,
                id: id,
                type: 'ENFANT_DE',
                token: TokenStorage.getToken(),
            }
        );

        return response;
    } catch (error) {
        return {
            message: 'add failed'
        }
    }
}

const addFirstPersonne = async ({ nom, prenom, dateNaissance, dateDeces, genre }) => {
    try {
        const id = Maths.guidGenerator();
        const response = await axios.post(`${API_URL}/api/personne/addFirstPersonne`,
            {
                nom,
                prenom,
                dateNaissance,
                dateDeces,
                genre,
                generationId: 0,
                id: id,
                token: TokenStorage.getToken(),
            }
        );

        return response;
    } catch (error) {
        return {
            message: 'add failed'
        }
    }
}

const addPartenaire = async ({ nom, prenom, personne, dateNaissance, dateDeces, genre }) => {
    try {
        const id = Maths.guidGenerator();
        const response = await axios.post(`${API_URL}/api/personne/addPartenaire`,
            {
                nom,
                prenom,
                dateNaissance,
                dateDeces,
                genre,
                personneId: personne.id,
                generationId: personne.generationId,
                id: id,
                type: 'MARIER',
                token: TokenStorage.getToken(),
            }
        );

        return response;
    } catch (error) {
        return {
            message: 'add failed'
        }
    }
}

const modifiePersonne = async ({ id, nom, prenom, dateNaissance, dateDeces, genre }) => {
    try {
        const response = await axios.post(`${API_URL}/api/personne/modifiePersonne`,
            {
                id,
                nom,
                prenom,
                dateNaissance,
                dateDeces,
                genre,
                token: TokenStorage.getToken(),
            }
        );

        return response;
    } catch (error) {
        return {
            message: 'modifie failed'
        }
    }
}

const deletePersonneMarier = async ({ id }) => {
    try {
        const response = await axios.post(`${API_URL}/api/personne/deletePersonneMarier`,
            {
                id: id,
                token: TokenStorage.getToken(),
            }
        );

        return response;
    } catch (error) {
        return {
            message: 'delete failed'
        }
    }
}

const deletePersonneCelibataire = async ({ id }) => {
    try {
        const response = await axios.post(`${API_URL}/api/personne/deletePersonneCelibataire`,
            {
                id: id,
                token: TokenStorage.getToken(),
            }
        );

        return response;
    } catch (error) {
        return {
            message: 'delete failed'
        }
    }
}

const getTree = async () => {
    try {
        const response = await axios.post(`${API_URL}/api/personne/getTree`,
            {
                token: TokenStorage.getToken()
            }
        );

        return response;
    } catch (error) {
        return {
            message: error.message
        }
    }
}

const uploadPhoto = async ({ files, personneId }) => {
    try {
        const formData = new FormData();
        formData.append('token', TokenStorage.getToken());
        formData.append('image', files[0]);
        formData.append('personneId', personneId);
        const response = await axios.post(`${API_URL}/api/upload/uploadPhoto`, formData, {
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        });

        return response;
    } catch (err) {
        return {
            message: err.response ? err.response.data.message : err.message
        }
    }
}

export default {
    addFirstPersonne,
    addDescendant,
    addPartenaire,
    modifiePersonne,
    deletePersonneMarier,
    deletePersonneCelibataire,
    getTree,
    uploadPhoto,
}